import subprocess, json, pathlib, requests, time
from flask import Blueprint, render_template, redirect, session
from firebase_admin import db as firebase 
from datetime import datetime, timedelta

views = Blueprint('views', __name__)

@views.route("/heart")
def heart():
    fitapi = "https://www.googleapis.com/fitness/v1/users/me/dataset:aggregate"
    file = pathlib.Path('/data/options.json')
    if file.is_file():
        # Opening JSON file
        f = open('/data/options.json') 
        options = json.load(f)
    else:
        f = open('options.json') 
        options = json.load(f)
    f.close()


    ref = firebase.reference("/")
    for key in ref.get():
        ref = firebase.reference(key)
        user = ref.get()

        if 'refresh' not in user:
            #skip and continue
            continue

        refreshdata={
        "client_id":options['Google 0auth'][0]['client_id'],
        "client_secret":options['Google 0auth'][0]['secret'],
        "refresh_token":user['refresh'],
        "grant_type":"refresh_token"
        }
        refreshdata = json.dumps(refreshdata)
        headers = { 'content-type': 'application/json;encoding=utf-8'}
        freshtoken = requests.post('https://accounts.google.com/o/oauth2/token',headers=headers, data=refreshdata)
        freshtoken = freshtoken.content.decode("utf-8")
        freshtoken = json.loads(freshtoken)

        if 'access_token' in freshtoken:
            ref.update({
            'token': freshtoken['access_token']
        })
        else:
            ref.delete()
            session.clear()
            #revoke user as well(missing)
            params={'token': user['refresh']}
            params = json.load
            revoke = requests.post('https://accounts.google.com/o/oauth2/revoke',headers=headers, params=params)
            print(revoke)
 
    #Generate time in miliseconds
    TODAY = datetime.today().date()
    NOW = datetime.today()
    START = int(time.mktime(TODAY.timetuple()) *1000)
    END = int(time.mktime(NOW.timetuple()) *1000)
    DAYDURATION = END - START
    
    WEEKSTART = (NOW - timedelta(NOW.weekday()+1)).replace(hour=12, minute=0, second=0, microsecond=0)
    WEEKSTART = int(time.mktime(WEEKSTART.timetuple()) *1000)
    WEEKEND = int(time.mktime(NOW.timetuple()) *1000)
    WEEKDURATION =  WEEKEND - WEEKSTART

    #Generate header for steps time range
    DAILYHEART = {"aggregateBy": [{
    "dataTypeName": "com.google.heart_minutes.summary",
    "dataSourceId": "derived:com.google.heart_minutes:com.google.android.gms:merge_heart_minutes"
    }],
    "bucketByTime": { "durationMillis": DAYDURATION },
    "startTimeMillis": START,
    "endTimeMillis": END
    }
    DAILYHEART = json.dumps(DAILYHEART)
    session['heartdaily'] = {}


    #Generate header for steps time range
    WEEKLYHEART = {"aggregateBy": [{
    "dataTypeName": "com.google.heart_minutes.summary",
    "dataSourceId": "derived:com.google.heart_minutes:com.google.android.gms:merge_heart_minutes"
    }],
    "bucketByTime": { "durationMillis": WEEKDURATION },
    "startTimeMillis": WEEKSTART,
    "endTimeMillis": WEEKEND
    }
    WEEKLYHEART = json.dumps(WEEKLYHEART)
    session["heartweekly"] = {}

    ref = firebase.reference("/")

    for key in ref.get():
        ref = firebase.reference(key)
        user = ref.get()

        headers = { 'content-type': 'application/json;encoding=utf-8','Authorization': 'Bearer %s' % user['token'] }
        session['fitdata'] = requests.post(fitapi, headers=headers, data=DAILYHEART)
        session['fitdata'] = session['fitdata'].content.decode("utf-8")

        fitjson = json.loads(session['fitdata'])

        if "fpVal" in session['fitdata']:
            session["heart"] = fitjson['bucket'][0]['dataset'][0]['point'][0]['value'][0]['fpVal']
        else:
            session["heart"] = 0
            
        session['heartdaily'][user['name']] = session["heart"]


        headers = { 'content-type': 'application/json;encoding=utf-8','Authorization': 'Bearer %s' % user['token'] }
        session['fitweekly'] = requests.post(fitapi, headers=headers, data=WEEKLYHEART)
        session['fitweekly'] = session['fitweekly'].content.decode("utf-8")

        fitweeklyjson = json.loads(session['fitweekly'])
        
        if "fpVal" in session['fitweekly']:
            weeklyheart = fitweeklyjson['bucket'][0]['dataset'][0]['point'][0]['value'][0]['fpVal']
        else:
            weeklyheart = 0
            
        session['heartweekly'][user['name']] = weeklyheart

    #Order steps by value
    session['heartweekly'] = dict(sorted(session['heartweekly'].items(), key=lambda x: x[1], reverse=True))
    session['heartdaily'] = dict(sorted(session['heartdaily'].items(), key=lambda x: x[1], reverse=True))

    return render_template('heart.html', heartdaily=session['heartdaily'], heartweekly=session['heartweekly'], name=session['name'], picture=session['picture'])


@views.route("/logout")
def logout():
    session.clear()
    return redirect("/")

@views.route("/policy")
def policy():
    return render_template('policy.html')

@views.route("/")
def index():
    if "google_id" not in session:
        return render_template('index.html')
    else:
        return redirect("/steps")

@views.route("/changelog")
def changelog():
    gitlog = subprocess.run(['git', 'log', '--pretty=format:"%h%x09%an%x09%ad%x09%s'],capture_output=True, text=True)

    return render_template('changelog.html', log=gitlog.stdout)

@views.route("/power")
def power():
    #https://developers.google.com/fit/datatypes/aggregate#rest_2
    return render_template('power.html')