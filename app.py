import os, pathlib
from sre_parse import State

import time
from datetime import datetime, timedelta
import requests, json
from flask import Flask, session, abort, redirect, request, render_template
from google.oauth2 import id_token
from google_auth_oauthlib.flow import Flow
from pip._vendor import cachecontrol
import google.auth.transport.requests
import firebase_admin
from firebase_admin import db as firebase 

app = Flask("Stepmaster")
app.secret_key = "stepmaster"

from views import views

app.register_blueprint(views, url_prefix='/')

fitapi = "https://www.googleapis.com/fitness/v1/users/me/dataset:aggregate"
os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"

file = pathlib.Path('/data/options.json')
if file.is_file():
    # Opening JSON file
    f = open('/data/options.json') 
    options = json.load(f)
else:
    f = open('options.json') 
    options = json.load(f)
f.close()

firebasecred = {
  "type": "service_account",
  "project_id": options['Firebase'][0]['project_id'],
  "private_key_id": options['Firebase'][0]['private_key_id'],
  "private_key": options['Firebase'][0]['private_key'],
  "client_email": options['Firebase'][0]['client_email'],
  "client_id": options['Firebase'][0]['firebase_client_id'],
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": options['Firebase'][0]['client_x509_cert_url']
}

google_api_credentials = {"web":{"client_id": options['Google 0auth'][0]['client_id'],
    "project_id":"stepmaster-353919",
    "auth_uri":"https://accounts.google.com/o/oauth2/auth",
    "token_uri":"https://oauth2.googleapis.com/token",
    "auth_provider_x509_cert_url":"https://www.googleapis.com/oauth2/v1/certs",
    "client_secret": options['Google 0auth'][0]['secret'],
    #"redirect_uris":["http://127.0.0.1/callback","http://localhost/callback"],
    #"javascript_origins":["https://stepmaster.baastrup.org"]
    }}

#save credentials
def credentials_to_dict(credentials):
  return {'token': credentials.token,
          'refresh_token': credentials.refresh_token,
          'token_uri': credentials.token_uri,
          'client_id': credentials.client_id,
          'client_secret': credentials.client_secret,
          'scopes': credentials.scopes}

#Can convert from miliseconds to date
def millistohuman(millisec):
    dt = datetime.fromtimestamp(millisec/1000)
    return dt.strftime('%Y-%m-%d %H:%M:%S')


flow = Flow.from_client_config(google_api_credentials,
scopes=["https://www.googleapis.com/auth/userinfo.profile", "https://www.googleapis.com/auth/userinfo.email", "openid", "https://www.googleapis.com/auth/fitness.activity.read"],
redirect_uri=options['Google 0auth'][0]['uri']
)

def login_is_required(function):
    def wrapper(*args, **kwargs):
        if "google_id" not in session:
            return abort(401)  # Authorization required

        else:
            return function()
    return wrapper

#firebase auth
cred = firebase_admin.credentials.Certificate(firebasecred)
firebase_admin.initialize_app(cred,{
	'databaseURL': 'https://stepmaster-353919-default-rtdb.europe-west1.firebasedatabase.app'
	})

@app.route("/login")
def login():
    authorization_url, state = flow.authorization_url(
    access_type='offline', #refresh access token
    include_granted_scopes='true')
    session["state"] = state

    return redirect(authorization_url)

@app.route("/callback")
def callback():
    flow.fetch_token(authorization_response=request.url)
    
    if not session["state"] == request.args["state"]:
        abort(500)  # State does not match!

    credentials = credentials_to_dict(flow.credentials)
    request_session = requests.session()
    cached_session = cachecontrol.CacheControl(request_session)
    token_request = google.auth.transport.requests.Request(session=cached_session)
    
    id_info = id_token.verify_oauth2_token(
        id_token=flow.credentials._id_token,
        request=token_request,
        audience=credentials['client_id'],
        clock_skew_in_seconds=30
    )

    session["google_id"] = id_info.get("sub") #used by login_is_required
    session["name"] = id_info.get("name")
    session["picture"] = id_info.get("picture")
    
    ref = firebase.reference(session['google_id'])

    ref.update({
        "token": credentials['token'],   
        "name": session["name"],
        "picture": session["picture"]
        }
    )

    if(credentials['refresh_token']):
        ref.update({
          'refresh': credentials['refresh_token']
    })

    return redirect("/")

@app.route("/steps")
@login_is_required
def steps():

    ref = firebase.reference("/")
    for key in ref.get():
        ref = firebase.reference(key)
        user = ref.get()

        if 'refresh' not in user:
            #skip and continue
            continue

        refreshdata={
        "client_id":options['Google 0auth'][0]['client_id'],
        "client_secret":options['Google 0auth'][0]['secret'],
        "refresh_token":user['refresh'],
        "grant_type":"refresh_token"
        }
        refreshdata = json.dumps(refreshdata)
        headers = { 'content-type': 'application/json;encoding=utf-8'}
        freshtoken = requests.post('https://accounts.google.com/o/oauth2/token',headers=headers, data=refreshdata)
        freshtoken = freshtoken.content.decode("utf-8")
        freshtoken = json.loads(freshtoken)

        if 'access_token' in freshtoken:
            ref.update({
            'token': freshtoken['access_token']
        })
        else:
            ref.delete()
            session.clear()
            #revoke user as well(missing)
            params={'token': user['refresh']}
            params = json.load
            revoke = requests.post('https://accounts.google.com/o/oauth2/revoke',headers=headers, params=params)
            print(revoke)
 
    #Generate time in miliseconds
    TODAY = datetime.today().date()
    NOW = datetime.today()
    START = int(time.mktime(TODAY.timetuple()) *1000)
    END = int(time.mktime(NOW.timetuple()) *1000)
    DAYDURATION = END - START
    
    WEEKSTART = (NOW - timedelta(NOW.weekday()+1)).replace(hour=12, minute=0, second=0, microsecond=0)
    WEEKSTART = int(time.mktime(WEEKSTART.timetuple()) *1000)
    WEEKEND = int(time.mktime(NOW.timetuple()) *1000)
    WEEKDURATION =  WEEKEND - WEEKSTART

    #Generate header for steps time range
    DAILYSTEPS = {"aggregateBy": [{
    "dataTypeName": "com.google.step_count.delta",
    "dataSourceId": "derived:com.google.step_count.delta:com.google.android.gms:estimated_steps"
    }],
    "bucketByTime": { "durationMillis": DAYDURATION },
    "startTimeMillis": START,
    "endTimeMillis": END
    }
    DAILYSTEPS = json.dumps(DAILYSTEPS)
    session['stepsdaily'] = {}


    #Generate header for steps time range
    WEEKLYSTEPS = {"aggregateBy": [{
    "dataTypeName": "com.google.step_count.delta",
    "dataSourceId": "derived:com.google.step_count.delta:com.google.android.gms:estimated_steps"
    }],
    "bucketByTime": { "durationMillis": WEEKDURATION },
    "startTimeMillis": WEEKSTART,
    "endTimeMillis": WEEKEND
    }
    WEEKLYSTEPS = json.dumps(WEEKLYSTEPS)
    session["stepsweekly"] = {}

    ref = firebase.reference("/")

    for key in ref.get():
        ref = firebase.reference(key)
        user = ref.get()

        headers = { 'content-type': 'application/json;encoding=utf-8','Authorization': 'Bearer %s' % user['token'] }
        session['fitdata'] = requests.post(fitapi, headers=headers, data=DAILYSTEPS)
        session['fitdata'] = session['fitdata'].content.decode("utf-8")

        fitjson = json.loads(session['fitdata'])

        if "intVal" in session['fitdata']:
            session["steps"] = fitjson['bucket'][0]['dataset'][0]['point'][0]['value'][0]['intVal']
        else:
            session["steps"] = 0
            
        session['stepsdaily'][user['name']] = session["steps"]


        headers = { 'content-type': 'application/json;encoding=utf-8','Authorization': 'Bearer %s' % user['token'] }
        session['fitweekly'] = requests.post(fitapi, headers=headers, data=WEEKLYSTEPS)
        session['fitweekly'] = session['fitweekly'].content.decode("utf-8")

        fitweeklyjson = json.loads(session['fitweekly'])
        
        if "intVal" in session['fitweekly']:
            weeklysteps = fitweeklyjson['bucket'][0]['dataset'][0]['point'][0]['value'][0]['intVal']
        else:
            weeklysteps = 0
            
        session['stepsweekly'][user['name']] = weeklysteps

    #Order steps by value
    session['stepsweekly'] = dict(sorted(session['stepsweekly'].items(), key=lambda x: x[1], reverse=True))
    session['stepsdaily'] = dict(sorted(session['stepsdaily'].items(), key=lambda x: x[1], reverse=True))

    return render_template('steps.html', stepsdaily=session['stepsdaily'], stepsweekly=session['stepsweekly'], name=session['name'], picture=session['picture'])

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)